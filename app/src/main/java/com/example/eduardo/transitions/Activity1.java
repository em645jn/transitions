package com.example.eduardo.transitions;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by Dedicables on 23/2/2017.
 */

public class Activity1 extends AppCompatActivity {
  Button button1;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity1);
    button1 = (Button) findViewById(R.id.button1);
    button1.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent i = new Intent(Activity1.this, Acvitity2.class);
        startActivity(i);
        overridePendingTransition(R.anim.slide_in_rigth, R.anim.slide_out_left);
      }
    });
  }
}
